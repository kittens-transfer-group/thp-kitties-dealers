Rails.application.routes.draw do
  devise_for :users
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root to: "items#index"
  resources :items, :path => "images", only: [:show] do 
    resources :pictures, only: [:create, :edit]
  end 
  post '/add_to_cart/:item_id' => 'carts#add_to_cart', :as => 'add_to_cart'
  resources :carts, only: [:show]
  resources :orders, only: [ :show], :path => "commandes"
  resources :users, only: [:show]
  resources :charges
  resources :transactions, only: [:create, :destroy]
  resources :mon_paniers, only: [:index], :path => "mon_panier"
  resources :mon_profils, only: [:index], :path => "mon_profil"
  
end
