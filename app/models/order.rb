class Order < ApplicationRecord
  # ASSOCIATIONS
  # N - 1 with users
  belongs_to :user
  
  # 1 - N with order_lines
  has_many :order_lines
  # N - N with items
  has_many :items, through: :order_lines

  # fill order at creation
  after_create :retrieve_cart_items

  # transfer all cart items in order as order lines
  def retrieve_cart_items
    self.user.cart.transactions.each do |transaction|
      self.order_lines.create!(
        order_id: self.id,
        item_id: transaction.item_id
      )
    end
  end

  # update the order's total amount
  def update_total
    self.total = 0
    self.order_lines.each do |order_line|
      self.total += order_line.item.price
    end
    self.save!()
  end
end
