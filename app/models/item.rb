class Item < ApplicationRecord
	validates :title, 
		presence: true,
		length: { minimum: 3 }
	validates :description,
		presence: true,
		length: { minimum: 40 }
	validates :price,
		presence: true,
		numericality: { greater_than_or_equal_to: 1 }
	validates :image_url,
		presence: true
  
  # ASSOCIATIONS
  # 1 - N with order_lines
  has_many :order_lines
  # N - N with orders
  has_many :orders, through: :order_lines

  # 1 - N with transactions
  has_many :transactions
  # N - N with carts
	has_many :carts, through: :transactions
  # active storage 
    has_one_attached :picture
end
