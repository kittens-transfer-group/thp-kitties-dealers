class Cart < ApplicationRecord
  belongs_to :user
  has_many :transactions
  has_many :items, through: :transactions

  def add_item(item_id)
      Transaction.create(cart_id: self.id, item_id: item_id)
  end

  def remove_item(item_id)
    t = Transaction.find_by(item_id: item_id)
    t.destroy
  end

  # clear the cart
  def empty_cart
    self.transactions.destroy_all
  end
end
