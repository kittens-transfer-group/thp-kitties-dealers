class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
  :recoverable, :rememberable, :validatable
  
  # ASSOCIATIONS
  # 1 - 1 with carts
  has_one :cart

  # 1 - N with orders
  has_many :orders

  after_create :create_cart

  private

  def create_cart
    self.cart = Cart.create!(user_id: self.id)
  end

end
