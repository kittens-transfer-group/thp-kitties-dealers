class OrderLine < ApplicationRecord
  # ASSOCIATIONS
  # N - 1 with orders
  belongs_to :order
  # N - 1 with items
  belongs_to :item
end
