class UserMailer < ApplicationMailer
  def ordered_email(user, order)
    @user = user
    @order = order
    # @order.orderlines.each do |order_line|
    #   picture = order_line.item.picture
    #   item_title = item.title.snakecase
    #   attachments.inline[item_title + '.jpg'] = picture
    # end
    
    mail(to: user.email, subject: 'Votre commande ' + order.stripe_customer_id)
  end
end
