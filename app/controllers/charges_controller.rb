class ChargesController < ApplicationController
  before_action :authenticate_user!, only: [:new, :create]
  def new
    @user = current_user
    @order = current_user.orders.create!()
    @items = current_user.cart.items
  end

def create
  
  # retrieve the order
  @order = current_user.orders.last
  @order.update_total
  # Amount in cents
  @amount = (@order.total * 100).to_i

  customer = Stripe::Customer.create({
    email: params[:stripeEmail],
    source: params[:stripeToken],
  })

  charge = Stripe::Charge.create({
    customer: customer.id,
    amount: @amount,
    description: 'Rails Stripe customer',
    currency: 'inr',
  })

  if charge
    @order.update!(stripe_customer_id: charge.customer)
    current_user.cart.empty_cart
    UserMailer.ordered_email(current_user, @order).deliver_now

    flash[:success] = "Paiement validé - commande validée - expédition en cours...\n
    Votre facture vous a été envoyé par email."
    redirect_to root_path
  else
    @order.destroy
    flash.now[:danger] = "Une erreur s'est produite - veuillez ré-essayer. Promis, on ne vous fera pas payer deux fois..."
    redirect_back(fallback_location: root_path)
  end

rescue Stripe::CardError => e
  flash[:error] = e.message
  redirect_to new_charge_path
end
end
