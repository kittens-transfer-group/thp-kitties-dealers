class CartsController < ApplicationController

    include CartsHelper
    before_action :current_cart, only: [:add_to_cart, :remove_from_cart]
    before_action :authenticate_user!, only: [:show, :remove_from_cart, :add_to_cart]

    def add_to_cart
        @item = Item.find(params[:item_id])
        current_cart.add_item(@item.id)
        flash[:success] = "La photo #{@item.title} a bien été ajoutée au panier"
        redirect_back(fallback_location: root_path)
    end

    def show
        current_user_must_own_cart
		@transactions = Cart.find(params[:id]).transactions
		@total_price = []
    end

    private

    def current_user_must_own_cart
        @cart = Cart.find(params[:id])
        if current_cart.id != @cart.id
            flash[:error] = "Vous n'avez pas accès à ce panier"
            redirect_to root_path
        end
    end

end
