class MonProfilsController < ApplicationController
  before_action :is_good_user?
  def index
  	@user = User.find(current_user.id)
  	@orders = @user.orders
  end

  private

  def is_good_user?
  	unless user_signed_in?
  		redirect_to root_path
  	end
  end
end
