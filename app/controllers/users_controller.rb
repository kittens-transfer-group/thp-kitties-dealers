class UsersController < ApplicationController
  before_action :authenticate_user!, only: [:show, :edit]

  def show
  	current_user_must_own_profile
  	@orders = User.find(params[:id]).orders
  	@user = User.find(params[:id])
  end

  private

  def current_user_must_own_profile
  	@user = User.find(params[:id])
  	if current_user.id != @user.id
  		flash[:error] = "Vous n'avez pas accès à ce profil"
  		redirect_to root_path
  	end
  end
end
