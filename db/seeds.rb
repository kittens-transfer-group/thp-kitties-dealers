# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

#####################
#####################
# WIPE THE DATABASE before seeding

def database_cleanup
  puts '*' * 40
  puts 'Database wipeout'
  puts '*' * 40 + "\n \n"

  models_list = [
    Item, User, Cart,
    OrderLine, Order
  ]

  models_list.each do |model|
    cleanup(model)
  end

  puts
  puts '*' * 40
  puts "Database wiped, oh yeah."
  puts '*' * 40
end

def cleanup(model)
  model.destroy_all
  puts '-' * 20
  puts model.to_s + ' : table cleaned up.'
end

#####################
#####################
# PLANT THE SEEDS...

# ----------
def item_seed
  new_item = Item.create!(
    title: Faker::Creature::Cat.breed,
    description: Faker::Lorem.sentence(word_count: 10),
    price: rand(3600..100000)*5/100.0, # must be at least 36 rupees for stripe compliance
    image_url: "https://loremflickr.com/g/600/600/snowboard?lock=#{rand(1..1000)}",
  )
  picture_file_name = 'snowboard_' + (format '%03d', rand(1..16)) + '.jpg'
  picture_path = Rails.root.join("app", "assets", "images", "item_pictures", picture_file_name)
  new_item.picture.attach(
    io: File.open(picture_path),
    filename: picture_file_name,
    content_type: "image/jpg"
    )
end

def items_seed
  puts "Seeding items"
  15.times do
    item_seed
  end
  puts Item.all.size.to_s + ' items created'
end
# ----------

# ----------
def main_user_seed
  User.create!(
    email: 'thp-kitten@yopmail.com',
    password: 'motdepasse',
    password_confirmation: 'motdepasse'
  )
end

def main_admin_seed
  User.create(
    email: 'thp-kitten-admin@yopmail.com',
    password: 'motdepasse',
    password_confirmation: 'motdepasse'
  )
end

def random_user_seed
  User.create(
    email: Faker::Internet.unique.email,
    password: 'motdepasse',
    password_confirmation: 'motdepasse'
  )
end

def users_seed
  puts "Seeding users"
  main_user_seed
  puts 'Main user created'

  main_admin_seed
  puts 'Main admin created'

  5.times do
    random_user_seed
  end
  puts (User.all.size - 2).to_s + ' random users created'
end
# ----------

# ----------
def orders_seed
  puts "Seeding orders"
  User.all.each do |user|
    rand(0..1).times do
      rand(1..5).times do
        user.orders.create!(
          stripe_customer_id: "ThisIsAFakeStripeCustomerIDForPlaceHoldingPurposes",
        )
      end
    end
  end
  puts Order.all.size.to_s + ' orders created'
end

def order_lines_seed
  puts "Seeding order lines"
  Order.all.each do |order|
    Item.all.sample(rand(1..5)).each do |item|
      order.order_lines.create!(item_id: item.id)
    end
    order.update_total()
  end
  puts Order.all.size.to_s + ' order lines created'
end
# ----------

#####################
#####################
# AND GET SOME WEEDS :)

def perform_seed
  database_cleanup
  puts
  puts "Beginning seed \n \n"
  items_seed
  puts
  users_seed
  puts
  orders_seed
  puts
  order_lines_seed
  puts
  puts '*' * 40
  puts "Seeding complete"
  puts '*' * 40
  puts
end

perform_seed
