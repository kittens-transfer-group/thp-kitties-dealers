class CreateOrderLines < ActiveRecord::Migration[5.2]
  def change
    create_table :order_lines do |t|

      # N - 1 association with orders table
      t.belongs_to :order, index: true
      # N - 1 association with items table
      t.belongs_to :item, index: true

      t.timestamps
    end
  end
end
