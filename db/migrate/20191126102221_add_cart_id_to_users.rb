class AddCartIdToUsers < ActiveRecord::Migration[5.2]
  def change
    change_table :users do |t|
      t.belongs_to :cart, index: true
    end 
  end
end
