class RemoveCartFromUsers < ActiveRecord::Migration[5.2]
  def change
    remove_reference :users, :cart, index: true
  end
end
