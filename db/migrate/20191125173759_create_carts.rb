class CreateCarts < ActiveRecord::Migration[5.2]
  def change
    create_table :carts do |t|
      t.belongs_to :user, index: true
      t.timestamps
    end

    create_table :transactions do |t|
      t.belongs_to :cart, index: :true 
      t.belongs_to :item, index: :true
      t.timestamps
    end
  end
end
