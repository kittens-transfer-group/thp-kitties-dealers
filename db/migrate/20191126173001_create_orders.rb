class CreateOrders < ActiveRecord::Migration[5.2]
  def change
    create_table :orders do |t|
      # link with Stripe
      t.string :stripe_customer_id

      # N - 1 association with users
      t.belongs_to :user, index: true

      t.timestamps
    end
  end
end
