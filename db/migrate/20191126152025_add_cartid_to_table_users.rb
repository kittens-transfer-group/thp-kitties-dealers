class AddCartidToTableUsers < ActiveRecord::Migration[5.2]
  def change
      add_reference :users, :cart, index: true
  end
end
